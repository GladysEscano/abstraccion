/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstraccion;
import Helpers.Costa_Rica_Presidente;
import Helpers.El_Salvador_Presidente;
import Helpers.Panama_Presidente;
/**
 *
 * @author Gladys Escaño
 */
public class Abstraccion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Costa_Rica_Presidente costarica = new Costa_Rica_Presidente();
        El_Salvador_Presidente salvador = new El_Salvador_Presidente();
        Panama_Presidente panama = new Panama_Presidente();
        
        System.out.println(costarica.getPais());
        System.out.println(costarica.getPresidente());
        
        System.out.println(salvador.getPais());
        System.out.println(salvador.getPresidente());
        
        System.out.println(panama.getPais());
        System.out.println(panama.getPresidente());
    } 
}

