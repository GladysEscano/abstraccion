/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author Gladys Escaño
 */
public abstract class Pais {
    
    //Declaraciones de métodos abstractos
    abstract String getPais();
    
    abstract String getPresidente();
}
